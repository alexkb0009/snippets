/*!
 * Background Freeform Slideshow v0.5.5
 * http://ics.com & http://alexb.ddns.me
 * Last modified July 4th, 2015
 *  - Changelog 09-28-2015:
 *      1) If option to set up progress icons is passed on slideshow initialization, active progress icon now sent as first 
 *         variable/parameter in event which is triggered on slide change.
 *  - Changelog 07-04-2015:
 *      1) Lots of stuff. New settings.
 *  - Changelog 01-19-2015:
 *      1) When w/o callback func and w/ supplied container element, set container's BG image before preloading other images.
 *  - Changelog 01-18-2015:
 *      1) Handling HTML per/within slide (include it in "image" objects, as "html", alongside "src" of image, in array passed
 *         to initialize function).
 *      2) Callback function no longer required, can pass in jQuery identifier for parent element of slideshow instead to be populated.
 *      3) Performance Improvements
 *      
 * Copyright (c) Alexander Balashov of Integrated Computer Solutions, Inc.
 * All rights reserved.
 *
 * ======================================================================
 * 
 * REQUIRES: jQuery (subject to change in future)
 *
 * USAGE:
 *
 * Initiate slideshow by calling function "freeFormSlideshow.preloadImagesForSlideshow" (see below) and supplying array 
 * of image URLs, settings, and callback.
 * In the callback, place slideshow elements and controls into the DOM where appropriate. Style in CSS stylesheet. Done.
 * May also use callback to hide (pre-)loading indicator, if one exists.
 */

 window.freeFormSlideshow = {

    /**
     * Preload Slideshow Images :: Initialization Function
     *
     * Calls function preloadImage for each image in array 'frontImageItems' 
     * which is created in firstslide-with-images.front-block.twig from content data.
     * Creates slideshow object if one doesn't already exists.
     * @param {String} identifier Unique identifier string to use/name this slideshow; used for CSS styling.
     * @param {Array} Array of Objects w/ URLs of images which to preload. Must be in the form: [ {src: 'URL_TO_IMAGE'}, {src: 'URL_TO_2nd_IMAGE'}, ... ]
     * @param {Function} callback (Optional) Function to execute once all images loaded, taking object of slides and control elements generated as input.
     * -- Callback function MUST contain one parameter, which will be provided w/ JS Object containing jQuery-bound elements specified in settings.
     * -- Second (optional) parameter for callback will have other (more temporary) cached elements, including containerElement (as wrapper) if was supplied in settings.
     * -- Elements generated and passed within object to callback function, for placement, include:
     *    - slidesContainer (full element)
     *    - containerElement (of slide HTML content) if input array has "html" attribute.
     *    - progressIcons
     *    - arrows
     *    
     * @param {Object} settings (Optional) An object with settings of/+ items to generate for the callback function:
     *   includeArrows: Boolean, default is true 
     *       -- Whether to generate next/previous arrows.
     *   includeProgressIcons: Boolean, default is true 
     *       -- Whether to generate progress icons.
     *   containerElement: String as identifier for jQuery selector OR jQuery-wrapped element, default is null 
     *       -- Use this in place of using a callback function.
     *   autoTransition: Boolean or Integer, default is false 
     *       -- Whether to initiate auto transitioning + timer (enter milliseconds of interval, if true).
     *   autoTransitionInitDelay: Integer, default is 0 
     *       -- How many milliseconds to wait before initializing auto-transition timer.
     *   transitionDuration: Integer, default is 1250 
     *       -- How long fade transition should take, in milliseconds.
     *         N.B.: DEPRECATED: Use CSS3 transitions instead. Kept in-case want to add JS transitions.
     *   initTransition: Boolean, default is false
     *       -- Whether to do initial transition right after load completion.
     *   createNavURLs: Boolean, default is false
     *       -- Whether to create 'href="#/slideshow-[identifier]/[slide]"' links for the progress icons.
     *   onlyRunInWindow: Boolean, default is true
     *       -- If true, will only transition when slideshow is in view, pausing the timer.
     *   separateImageElement: Boolean, default is false
     *       -- If true, separate element with image background will be created, to use next to content, 
     *          rather than as the background image of content. Useful for CSS3 transitions of backgrounds, etc.
     *   startAtSlide: Integer, default is 0
     *       -- Where to start the slideshow.
     */

    preloadImagesAndInitialize: function(identifier, imgURLArray, callback, settings){
    
      // Set jQuery if not initiated as "$" yet (e.g. in Drupal).
      window.$ = ($ || jQuery);
      
      // ERROR HANDLING/SETUP:
      
      if (typeof imgURLArray == 'undefined' || imgURLArray.length < 1){
        log('No slideshow image URLs provided. Exiting.');
        return false;
      }
      
      // Correct or set default settings.
      settings = freeFormSlideshow.checkValidateSettings(settings);
      
      // Create/check 'slideshows' object to store data for each slideshow.
      if (typeof freeFormSlideshow.slideshows == 'undefined'){
        log('No slideshow variable exists for storing of preloaded images. Creating one.');
        freeFormSlideshow.slideshows = {};
      } 
      
      // If this slideshow (identifier) hasn't been initialized yet, define its variables / data-containers. Some may be set later in execution.
      // Main purpose of this is to define an impromptu 'interface' for the data and how it may be used or accessed.
      if (typeof freeFormSlideshow.slideshows[identifier] == 'undefined'){
        freeFormSlideshow.slideshows[identifier] = {
          inputSlides: imgURLArray,
          images: [],   // Preloaded image ELEMENTS.
          html: [],     // HTML markup for slides, if any
          titles: [],   // Slide titles, e.g. for navigation, if any.
          currentSlideIndex: settings.startAtSlide,
          timesTriggeredManually: 0,
          transitionDuration: settings.transitionDuration,
          transitionTimeout: null,
          transitionEvent : $.Event('slideTransition', {'identifier' : identifier}),
          slideTimer: null,
          startAtSlide: settings.startAtSlide,
          startLoadTime: (new Date()).getTime()
        };
      }
      
      // Create cachedElements if non-existant.
      if (typeof cachedElements == 'undefined')                         window.cachedElements = {};
      if (typeof cachedElements.slideshows == 'undefined')              cachedElements.slideshows = {};
      if (typeof cachedElements.slideshows[identifier] == 'undefined')  cachedElements.slideshows[identifier] = {};

      // Create + insert first slide, before others, as a "background".
      if (settings.containerElement != null){
        freeFormSlideshow.tmp.firstSlide = imgURLArray[settings.startAtSlide]; /* (imgURLArray.splice(settings.startAtSlide, 1))[0]; */ /* imgURLArray.shift(); */
        cachedElements.slideshows[identifier].wrapper = settings.containerElement;
        
        freeFormSlideshow.preloadImage(identifier, freeFormSlideshow.tmp.firstSlide.src, null, function(){
          cachedElements.slideshows[identifier].wrapper.trigger($.Event('firstSlideImageLoaded', {'identifier' : identifier}));
          setTimeout(function(){
            cachedElements.slideshows[identifier].wrapper.addClass('first-slide-loaded');
          }, ((new Date()).getTime() - freeFormSlideshow.slideshows[identifier].startLoadTime > 1000 ? 0 : 1000)); 
          
        });
        
        freeFormSlideshow.tmp.firstSlideContentString  = '<div class="initial-content-wrapper active"';
        if (!settings.separateImageElement) freeFormSlideshow.tmp.firstSlideContentString += 'style="background-image: url(' + freeFormSlideshow.tmp.firstSlide.src + ');"';
        freeFormSlideshow.tmp.firstSlideContentString += '>';
        if (settings.separateImageElement) {
          freeFormSlideshow.tmp.firstSlideContentString += '<div class="image-elem" style="background-image: url(' + freeFormSlideshow.tmp.firstSlide.src + ');">&nbsp;</div>';
        } else {
          freeFormSlideshow.tmp.firstSlideContentString += '&nbsp;';
        }
        if (typeof freeFormSlideshow.tmp.firstSlide.html != 'undefined'){
          freeFormSlideshow.tmp.firstSlideContentString += '<div class="slide-content initial">' + freeFormSlideshow.tmp.firstSlide.html + '</div>';
        }
        freeFormSlideshow.tmp.firstSlideContentString += '</div>';
        cachedElements.slideshows[identifier].wrapper.append(freeFormSlideshow.tmp.firstSlideContentString);
        cachedElements.slideshows[identifier].firstSlide = cachedElements.slideshows[identifier].wrapper.children('.initial-content-wrapper');
      }
      
      // Start preloading images
      log('Preloading images for ' + identifier + '...');
      
      freeFormSlideshow.tmp.i = 0;
      while (freeFormSlideshow.tmp.i < imgURLArray.length){
        if (freeFormSlideshow.tmp.i == settings.startAtSlide) {
          freeFormSlideshow.tmp.i++;
          continue;
        }
        freeFormSlideshow.preloadImage(identifier, imgURLArray[freeFormSlideshow.tmp.i]['src'], imgURLArray.length - 1, callback, settings, freeFormSlideshow.tmp.i);
        if (typeof imgURLArray[freeFormSlideshow.tmp.i]['html'] != 'undefined'){
          freeFormSlideshow.slideshows[identifier].html[freeFormSlideshow.tmp.i] = imgURLArray[freeFormSlideshow.tmp.i]['html'];
        }
        if (typeof imgURLArray[freeFormSlideshow.tmp.i]['title'] != 'undefined'){
          freeFormSlideshow.slideshows[identifier].titles[freeFormSlideshow.tmp.i] = imgURLArray[freeFormSlideshow.tmp.i]['title'];
        }
        freeFormSlideshow.tmp.i++;
      }
      
    },
    
    /**
     * Function that validates settings & sets defaults when needed, returning a well-formed settings object.
     * 
     * @param {Object} settings (Optional) Object to validate & correct; if none given, one is created.
     * @return {Object} Correct settings object.
     */
    
    checkValidateSettings: function(settings){
      
      // Create settings if none exist.
      if (typeof settings == 'undefined' || settings == null || !settings) settings = {};
      
      if (typeof settings.transitionDuration    != 'number')    settings.transitionDuration = 1250;
      if (typeof settings.includeArrows         != 'boolean')   settings.includeArrows = true;
      if (typeof settings.includeProgressIcons  != 'boolean')   settings.includeProgressIcons = true;
      if (typeof settings.containerElement      == 'undefined') settings.containerElement = null;
      if (typeof settings.containerElement      == 'string')    settings.containerElement = $(settings.containerElement);
      if (typeof settings.autoTransition        != 'number') {
        if (typeof settings.autoTransition  == 'undefined')         settings.autoTransition = false;
        else if (settings.autoTransition    == true)                settings.autoTransition = 6000;
      }
      if (typeof settings.autoTransitionInitDelay != 'number')  settings.autoTransitionInitDelay = 0;
      if (typeof settings.initTransition        != 'boolean')   settings.initTransition = false;
      if (typeof settings.createNavURLs         != 'boolean')   settings.createNavURLs = false;
      if (typeof settings.onlyRunInWindow       != 'boolean')   settings.onlyRunInWindow = true;
      if (typeof settings.separateImageElement  != 'boolean')   settings.separateImageElement = false;
      if (typeof settings.startAtSlide          != 'number')    settings.startAtSlide = 0;
      
      return settings;
    },


    /**
     * Function for preloading one off-screen image. On finishing, calls callback function with resulting, loaded Image object.
     *
     * @param {String} identifier Identifier string of slideshow.
     * @param {String} imageURL URL to image of which to get size.
     * @param {Integer} limit (Optional) Limit to pass to saveToImagesArray to determine when to start processing again (on all loaded).
     *           -- If no limit supplied, doesn't add to final array and instead runs callback func. 
     * @param {Function} callback Reference to callback function to call with dimensions as params. 
     *           -- Callback must take an object with width and height properties UNLESS is .
     * @param {Object} settings (Optional) An object with settings of/+ items to generate for the callback function.
     * @param {Integer} index (Optional) index, used by callback, e.g. for ordering loaded images.
     */

    preloadImage: function(identifier, imageURL, limit, callback, settings, index){

      if (typeof cachedElements.preloadContainer == 'undefined' && document.getElementById('preloadContainer') == null) {
        $('<div id="preloadContainer" style="display:block;position:absolute;opacity:0.001;">').prependTo(document.body); // Mobile-fixed preloading. 
        cachedElements.preloadContainer = $('#preloadContainer');
      }

      var img = new Image();
      img.src = imageURL;
      img.style.height = "1px";
      img.style.width = "1px";
      $(img).appendTo(cachedElements.preloadContainer);
      $(img).load(function(){
        if (typeof limit == 'number' && typeof index === 'number' && index % 1 === 0) {        // If index + limit exists, include them.
          freeFormSlideshow.saveToImagesArray(identifier, img, limit, callback, settings, index); 
        } else if (typeof limit == 'number') {
          freeFormSlideshow.saveToImagesArray(identifier, img, limit, callback, settings);
        } else if (typeof callback == 'function'){
          // If no limit supplied (e.g. is null), don't add to final array and just run callback func given.
          callback(this);
        }
      });
    },

    /**
     * Callback Function for preloadImage, to save to persistent array.
     *
     * @param {String} identifier Identifier string of slideshow.
     * @param {Image} image A JS Image object which has already been preloaded into page. 
     * @param {Integer} limit # of images expected to be loaded. On reaching this number of images loaded, script processing continues.
     * @param {Function} callback (Optional) Function to execute once all images loaded.
     * @param {Object} settings (Optional) An object with settings of items to generate
     * @param {Integer} index (Optional) 0-based index or ordering of image to put into output array.
     */

    saveToImagesArray: function(identifier, image, limit, callback, settings, index){
    
      if (typeof index === 'number' && index % 1 === 0) {
        freeFormSlideshow.slideshows[identifier].images[index] = image;    // If ordered (index supplied). Morphs into Array-like Object.
      } else {
        freeFormSlideshow.slideshows[identifier].images.push(image);       // If order determined by which image loaded first. Retains Array type.
      }

      if (freeFormSlideshow.slideshows[identifier].images.length >= limit) { // After all images preloaded, do this:
        
        // DOUBLE-CHECK if all items are loaded. 
        // Because of array key alignment, some array keys may not have values (preloaded image URLs) yet;
        // as preloaded images are loaded asynchronously, the last image might load before others.
        freeFormSlideshow.tmp.i = limit;
        while(freeFormSlideshow.tmp.i > -1){
          if (freeFormSlideshow.tmp.i == settings.startAtSlide){
            freeFormSlideshow.tmp.i--;
            continue;
          }
          if (typeof freeFormSlideshow.slideshows[identifier].images[freeFormSlideshow.tmp.i] == 'undefined') return false; // Nope not all loaded yet. Cancel!
          freeFormSlideshow.tmp.i--;
        }
        
        // If all items have been confirmed loaded, continue to generate slideshow layers, etc. 
        log('Loaded ' + limit + ' images for ' + identifier + ' at index ' + index + '.');
        freeFormSlideshow.generateSlideshow(identifier, callback, settings);
        return true;
        
      }
      return false; // If not all preloaded yet.
    },
    
    /**
     * Function used in initialization of slideshow. Called once, after all images confirmed to be pre-loaded.
     * Handles DOM element creation & insertion in response to settings provided initially.
     * 
     * @param {String} identifier Unique name of this slideshow, passed down from initialization.
     * @param {Function} callback (Optional) Function to execute once all images loaded & slideshow is initialized, passed down from initialization.
     * @param {Object} settings (Optional) An object with settings of slideshow, passed down from initialization.
     */
    
    generateSlideshow: function(identifier, callback, settings){
    
      // Generate slideshow layers/elements.
      freeFormSlideshow.tmp.slideshowElements = freeFormSlideshow.generateSlideshowLayers(identifier, settings);
      
      // Transition immediately on load to second slide, if allowed by settings. If initDelay is set, wait that long first.
      if (settings.initTransition) setTimeout(freeFormSlideshow.slideTransition, 0, identifier);
      
      // Setup timed (auto) transitioning, if allowed by settings.
      if (settings.autoTransition){
        setTimeout(function(identifier, settings){
          freeFormSlideshow.slideshows[identifier].slideTimer = setInterval(function(){
          
            if (typeof windowActive != 'undefined' && !windowActive) return false; // Cancel if window not active (save CPU cycles).
            if (settings.onlyRunInWindow){ // Cancel if out of browser viewport. Set setting to false to disable, e.g. to align with progress bars.
              var slideshowVerticalPositionOnPage = cachedElements.slideshows[identifier].slidesContainer.offset().top;
              if (
                document.body.scrollTop < slideshowVerticalPositionOnPage - parseInt(window.innerHeight / 2) ||
                document.body.scrollTop > slideshowVerticalPositionOnPage + parseInt(window.innerHeight / 2)
              ) return false;
            }
            freeFormSlideshow.slideTransition(identifier);
            
          }, typeof settings.autoTransition == 'number' ? settings.autoTransition : 7500);
        }, (!settings.initTransition ? settings.autoTransitionInitDelay : 0), identifier, settings);
      }
      
      
      // Manually place stuff via callback function, if no container element is supplied.
      if (typeof callback === 'function' && settings.containerElement == null) {
        callback(freeFormSlideshow.tmp.slideshowElements, cachedElements);
        
      // Auto place navigation stuff within supplied wrapper if one is supplied, then run callback function (if also supplied).
      } else if (settings.containerElement != null){ 

        cachedElements.slideshows[identifier].wrapper.append(freeFormSlideshow.tmp.slideshowElements.slidesContainer);
  
        if (typeof freeFormSlideshow.tmp.slideshowElements.arrows != 'undefined') {
          freeFormSlideshow.tmp.slideshowElements.slidesContainer.after(freeFormSlideshow.tmp.slideshowElements.arrows);
        }

        if (typeof freeFormSlideshow.tmp.slideshowElements.progressIcons != 'undefined') {
          freeFormSlideshow.tmp.slideshowElements.slidesContainer.after(freeFormSlideshow.tmp.slideshowElements.progressIcons);
        }

        if (typeof callback === 'function') callback(freeFormSlideshow.tmp.slideshowElements, cachedElements);
          
      } else {
        log("Error: No container element nor callback functions for UI items supplied.");
        log("You may still use generated slideshow elements, available on this page in JS object: 'freeFormSlideshow.tmp.slideshowElements'.");
      }
    },

    /**
     * @param {String} identifier Unique name of slideshow.
     * @param {Object} settings (Optional) An object with settings of items to generate:
     */

    generateSlideshowLayers: function(identifier, settings){

      // Create background image elements.
      var slideshowElements = {
        // At minimum, slide container is created.
        slidesContainer: document.createElement('div')
      }; 
      slideshowElements.slidesContainer.className = 'slide-container ' + identifier;
      slideshowElements.slidesContainer.id = 'slideshow-' + identifier;
      var tempText;
      for (freeFormSlideshow.tmp.i = 0; freeFormSlideshow.tmp.i < freeFormSlideshow.slideshows[identifier].inputSlides.length; freeFormSlideshow.tmp.i++){
        if (freeFormSlideshow.tmp.i == settings.startAtSlide) continue;
        tempText = '<div class="slide-image num-' + freeFormSlideshow.tmp.i + '" ';
        if (!settings.separateImageElement) {
          tempText += 'style="background-image: url(' + freeFormSlideshow.slideshows[identifier].inputSlides[freeFormSlideshow.tmp.i].src + ');"';
        }
        tempText += '>';
        if (settings.separateImageElement) {
          tempText += '<div class="image-elem" style="background-image: url(' + freeFormSlideshow.slideshows[identifier].inputSlides[freeFormSlideshow.tmp.i].src + ');">&nbsp;</div>';
        } else {
          tempText += '&nbsp;'
        }
        tempText += '</div>';
        slideshowElements.slidesContainer.innerHTML += tempText;
      }
      slideshowElements.slidesContainer = $(slideshowElements.slidesContainer); // jQuerify
      
      // Utility func.
      function hrefString(num){
        if (typeof num != 'number') num = '';
        return settings.createNavURLs ? 'href="#slideshow-' + identifier + '/' + num + '"' : '';
      }
      
      // Create arrow buttons, if allowed by settings.
      if (settings.includeArrows) {
        slideshowElements.arrows  = document.createElement('div');
        slideshowElements.arrows.className = 'arrows-container ' + identifier;
        slideshowElements.arrows.innerHTML += '<a ' + hrefString() + ' class="left slide-button"></a>';
        slideshowElements.arrows.innerHTML += '<a ' + hrefString() + ' class="right slide-button"></a>';
        slideshowElements.arrows = $(slideshowElements.arrows); // jQuerify
      }
      
      // Create progress buttons, if allowed by settings.
      if (settings.includeProgressIcons) {
        slideshowElements.progressIcons  = document.createElement('div');
        slideshowElements.progressIcons.className = 'progress-icons-container ' + identifier;
        
        var innerProgressContainer = document.createElement('div');
        innerProgressContainer.className = 'progress-icons-inner-container';
        
        // Start at -1 so as to include first slide (background image). Leave out of loop to add 'active' class.
        //innerProgressContainer.innerHTML += '<a ' + hrefString(1) + ' class="progress-icon num--1 active" slidenumber="-1" title="' + freeFormSlideshow.tmp.firstSlide.title + '"></div>';
        for (freeFormSlideshow.tmp.i = 0; freeFormSlideshow.tmp.i < freeFormSlideshow.slideshows[identifier].inputSlides.length; freeFormSlideshow.tmp.i++){
          innerProgressContainer.innerHTML += '<a ' + hrefString(freeFormSlideshow.tmp.i + 1) + ' class="progress-icon num-' + freeFormSlideshow.tmp.i + ' ' + (freeFormSlideshow.tmp.i == settings.startAtSlide ? 'active' : '') +  '" slidenumber="' + freeFormSlideshow.tmp.i + '" title="' + freeFormSlideshow.slideshows[identifier].inputSlides[freeFormSlideshow.tmp.i].title + '"></div>';
        }
        slideshowElements.progressIcons.innerHTML += '</div>';
        slideshowElements.progressIcons.appendChild(innerProgressContainer);
        slideshowElements.progressIcons = $(slideshowElements.progressIcons); // jQuerify
      }
      
      $.extend(cachedElements.slideshows[identifier], slideshowElements);
      cachedElements.slideshows[identifier].slides = cachedElements.slideshows[identifier].slidesContainer.children('div.slide-image');
      
      // Add HTML to each slide, if it exists.
      freeFormSlideshow.tmp.i = 0;
      while (freeFormSlideshow.tmp.i < freeFormSlideshow.slideshows[identifier].inputSlides.length){
        if (freeFormSlideshow.tmp.i == freeFormSlideshow.slideshows[identifier].startAtSlide){
          freeFormSlideshow.tmp.i++;
          continue;
        }
        (cachedElements.slideshows[identifier].slides.filter('.num-' + freeFormSlideshow.tmp.i)[0]).innerHTML += '<div class="slide-content num-' +  freeFormSlideshow.tmp.i + '">' + freeFormSlideshow.slideshows[identifier].inputSlides[freeFormSlideshow.tmp.i].html + '</div>';
        freeFormSlideshow.tmp.i++;
      };

      // Function/Transition Click Bindings
      if (typeof cachedElements.slideshows[identifier].arrows != 'undefined'){
        cachedElements.slideshows[identifier].arrows.children('.slide-button').click(function(){
          freeFormSlideshow.manualSlideTransition(identifier, 'Arrow', null, $(this).hasClass('left') ? -1 : 1);
        });
      }
      if (typeof cachedElements.slideshows[identifier].progressIcons != 'undefined'){
        cachedElements.slideshows[identifier].progressIcons.children('.progress-icons-inner-container').children('.progress-icon').click(function(){
          freeFormSlideshow.manualSlideTransition(identifier, 'ProgressIcon', parseInt(this.getAttribute('slidenumber')));
        });
      }
      
      return cachedElements.slideshows[identifier];
    },


    manualSlideTransition: function(identifier, controlType, indexOfTargetSlide, increment){
      
      if (freeFormSlideshow.slideshows[identifier].slideTimer != null){
        window.clearInterval(freeFormSlideshow.slideshows[identifier].slideTimer);
        delete freeFormSlideshow.slideshows[identifier].slideTimer;
      }
      
      freeFormSlideshow.slideshows[identifier].timesTriggeredManually++;
      
      if (typeof indexOfTargetSlide == 'undefined' || indexOfTargetSlide == null){
        if (typeof increment != 'undefined' && increment != null) {
          freeFormSlideshow.slideTransition(identifier, increment, null, true);
        } else {
          freeFormSlideshow.slideTransition(identifier, null, null, true); // Fallback.
        }
      } else {
        freeFormSlideshow.slideTransition(identifier, null, indexOfTargetSlide, true); // Go to specified slide by index.
      }
      
      /** 
        *   Google Analytics Event Tracking (if enabled)
       **/
      
      if (typeof controlType == 'undefined') controlType = 'NotSpecified';

      if (typeof _gaq != 'undefined'){
        // for ga.js (Classic)
        _gaq.push(['_trackEvent', 'FrontPageEvents', 'Click in Slideshow', controlType + ' - ' + identifier, freeFormSlideshow.slideshows[identifier].timesTriggeredManually]);
      }
      
      if (typeof ga == 'function'){
        // for analytics.js (Universal)
        ga('send', 'event', 'FrontPageEvents', 'Click in Slideshow', controlType + ' - ' + identifier, freeFormSlideshow.slideshows[identifier].timesTriggeredManually);
      }
    },


    slideTransition: function(identifier, increment, targetIndex, triggeredManually){
      
      // Error Handling
      if (typeof cachedElements.slideshows[identifier].slides == 'undefined'){ 
        log("Oops, no slide images loaded into DOM.");
        return false;
      }
      if (triggeredManually !== true) triggeredManually = false;
      
      // Initialize if has not been yet.
      if (typeof freeFormSlideshow.slideshows[identifier].currentSlideIndex == 'undefined' || freeFormSlideshow.slideshows[identifier].currentSlideIndex == null){ 
        freeFormSlideshow.slideshows[identifier].currentSlideIndex = 0;
      }

      // If no target index, check for increment; if no increment, fall back to +1
      if (typeof targetIndex == 'undefined' || targetIndex == null) {
        if (typeof increment == 'undefined') increment = 1; // Default increment = +1 (e.g. for auto rotation).
        targetIndex = freeFormSlideshow.slideshows[identifier].currentSlideIndex + increment;
      }

      freeFormSlideshow.slideshows[identifier].currentSlideIndex = targetIndex;
      
      // Loop Closing
      if (freeFormSlideshow.slideshows[identifier].currentSlideIndex >= freeFormSlideshow.slideshows[identifier].inputSlides.length) { 
        freeFormSlideshow.slideshows[identifier].currentSlideIndex = 0; // -1 so current slide = initial background (no slide) and @ next call will be index 0 (first slide).
        log('uhoh bigger than limit');
      } else if (freeFormSlideshow.slideshows[identifier].currentSlideIndex < 0){ 
        freeFormSlideshow.slideshows[identifier].currentSlideIndex = (freeFormSlideshow.slideshows[identifier].inputSlides.length - 1); // Loop back to last slide.
      } 
      
      log('Navigated to slide ' + freeFormSlideshow.slideshows[identifier].currentSlideIndex + ' in slideshow:' + identifier);
      
      /** Slide Element Transitioning **/
      
      // Cancel current timeout if initiating transition before it has ended in order to prevent queue build-up or other unintended effects.
      // Hide/reset opacity of all slides except the last active.
      // Keep last active slide up for remainder of timeout to allow transition effect to complete.
      
      if (typeof freeFormSlideshow.slideshows[identifier].transitionTimeout != 'undefined' && freeFormSlideshow.slideshows[identifier].transitionTimeout != null){
        
        clearTimeout(freeFormSlideshow.slideshows[identifier].transitionTimeout);
        
        if (typeof cachedElements.slideshows[identifier].slidesToHide != 'undefined'){
          cachedElements.slideshows[identifier].slidesToHide.not('.active').css('opacity','0');
          freeFormSlideshow.slideshows[identifier].transitionTimeout = setTimeout(function(lastSlideToHide){
            lastSlideToHide.css('opacity','0');
          }, freeFormSlideshow.slideshows[identifier].transitionDuration, cachedElements.slideshows[identifier].slidesToHide.has('.active'));
        }

      }
      
      // Perform slightly different things if going back to slide index -1 (different slide container, etc.).
      if (freeFormSlideshow.slideshows[identifier].currentSlideIndex == freeFormSlideshow.slideshows[identifier].startAtSlide){ 
        // Exited SlideShow (on first image)
        cachedElements.slideshows[identifier].slidesContainer.removeClass('in-slideshow');
        cachedElements.slideshows[identifier].firstSlide.addClass('active');
        cachedElements.slideshows[identifier].slidesToHide = cachedElements.slideshows[identifier].slides;
        delete cachedElements.slideshows[identifier].slideToShow;
      } else { 
        // In SlideShow
        if (!cachedElements.slideshows[identifier].slidesContainer.hasClass('in-slideshow')) cachedElements.slideshows[identifier].slidesContainer.addClass('in-slideshow');
        cachedElements.slideshows[identifier].slidesToHide = cachedElements.slideshows[identifier].slides.not('.num-' + freeFormSlideshow.slideshows[identifier].currentSlideIndex/*cachedElements.slideshows[identifier].slides[freeFormSlideshow.slideshows[identifier].currentSlideIndex]*/);
        cachedElements.slideshows[identifier].slideToShow = cachedElements.slideshows[identifier].slides.filter('.num-' + freeFormSlideshow.slideshows[identifier].currentSlideIndex);
        cachedElements.slideshows[identifier].slideToShow.addClass('active').css('opacity','1'); 
        cachedElements.slideshows[identifier].firstSlide.removeClass('active');
      }
      
         
      cachedElements.slideshows[identifier].slidesToHide.removeClass('active');  
      freeFormSlideshow.slideshows[identifier].transitionTimeout = setTimeout(function(){
        cachedElements.slideshows[identifier].slidesToHide.css('opacity','0');
        freeFormSlideshow.slideshows[identifier].transitionTimeout = null;
      }, freeFormSlideshow.slideshows[identifier].transitionDuration);
        
      
      // Adjust Progress Icons
      var progressIconsContainer = (cachedElements.slideshows[identifier].progressIcons || $('.progress-icons-container.' + identifier));
      var newlyActiveProgressIcon = null;
      if (progressIconsContainer.length > 0){
        newlyActiveProgressIcon = progressIconsContainer.find('.progress-icon.num-' + freeFormSlideshow.slideshows[identifier].currentSlideIndex);
        progressIconsContainer.children('.progress-icons-inner-container').children('.progress-icon').removeClass('active');
        newlyActiveProgressIcon.addClass('active');
      }
      
      // Emit slideTransition event (can be used to trigger stuff) on progressIcon.
      // Use progress icon as emitter if it exists.
      if (newlyActiveProgressIcon != null){
        newlyActiveProgressIcon.trigger(freeFormSlideshow.slideshows[identifier].transitionEvent, [newlyActiveProgressIcon, freeFormSlideshow.slideshows[identifier].currentSlideIndex, triggeredManually]);
      } else {
      // Otherwise, use slides container.
        cachedElements.slideshows.header.slidesContainer.trigger(freeFormSlideshow.slideshows[identifier].transitionEvent, [null, freeFormSlideshow.slideshows[identifier].currentSlideIndex, triggeredManually]);
      }
      
      // Set HTML stuff. 
      
      /** ToDo: Implement cool transition effects which can be set in settings. **/
      
    },
    
    tmp : {
        i: null
    } // Temporary items held during init and such.
}

/** EXTRA: Cross-Browser Safe Logging & Debugging Function **/

function log(msg){
  // return false; // (Un)comment this line for debugging purposes.
  try {
    if (console){
      console.log(msg);
      return true;
    } else return false;
  } catch (e) {
    return false;
  }
}